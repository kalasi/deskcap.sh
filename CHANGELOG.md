# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning][semver].

## [Unreleased]

### Added

### Changed
- Fixed repo links in changelog

## [1.0.2] - 2016-01-19

### Changed

### Added
- Added a changelog available at [./CHANGELOG.md]

## [1.0.1] - 2016-01-19

### Changed
- Compacted help statement to <= 80-chars-per-line

### Added

## v1.0.0 - 2016-01-09

Initial commit.

[Unreleased]: https://gitlab.com/kalasi/deskcap.sh/compare/v1.0.2...master
[1.0.2]: https://gitlab.com/kalasi/deskcap.sh/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/kalasi/deskcap.sh/compare/v1.0.0...v1.0.1
[./CHANGELOG.md]: https://gitlab.com/kalasi/deskcap.sh/master/CHANGELOG.md
[semver]: http://semver.org
