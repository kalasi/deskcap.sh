#!/usr/bin/env bash
#
# Description:  Records a window or the desktop and converts the video to webm
#               format.
# Required dependencies:
# - ffmpeg: record desktop monitor or window
#
# Optional dependencies:
# - xwininfo: retrieve information about the selected window
# - libnotify: Notify about information straight to desktop
# - keybind in your WM to "killall ffmpeg" (like ctrl+alt+X)
#
# Usage:
# $ sh screencast [options] [-h | --help]

# Check if a command exists by name given the first argument.
# Using `command -v CMD` is more powerful over `which`, because it's an
# external process and is not a builtin to bash, and some OS don't have exit
# codes for `which`.
command_exists() {
    command -v "$1" > /dev/null 2>&1 || {
        return 1
    }

    return 0
}

msg() {
    EXISTS=command_exists "notify-send"

    if [[ "$EXISTS" -eq 1 ]]; then
        notify-send "$1"
    else
        echo "$1"
    fi
}

MODE="desktop"         # Default "desktop" ["window"|"desktop"]
MARGIN=0               # Margin in window mode (px)
TITLEBAR=0             # Titlebar height in window mode (px)
FPS=30                 # Frames Per Second [12-60]
PRESET="ultrafast"     # Default "ultrafast" (x264 --help for preset list)
CRF=10                 # Constant Rate Factor [0-51] (Lower is better quality)
QMAX=10                # Maximum Quantization [1-31] (Lower is better quality)
FULLSCREEN="1920x1080" # Set your desktop resolution

# The default OUTPUT is simply a modified ISO timestamp of the current time
# in the home dir.
DATE=$(date -d "@$(date +%s)" +"%Y_%m_%d_%H_%M_%S")
OUTPUT="${HOME}/${DATE}" # Final webm destination.

# Temporary location of video created by ffmpeg.
# This is a random filename with characters a-f and 0-9, 32 chars long.
# This is not simply 'out.webm' or similar to avoid replacing a file in `~`.
TMP="$HOME/$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 32)_screencast.mkv"

if [[ "$1" = "-h" ]] || [[ "$1" = "--help" ]]; then
    echo -e "Useage: $ sh screencast [OPTIONS]\n"
    echo "  -h|--help               Display this help"
    echo "  -m|--mode desktop       Set mode [\"desktop\"|\"window\"]"
    echo "  -s|--space 10           Set margin in window mode (px)"
    echo "  -t|--title 24           Set titlebar height in window mode (px)"
    echo "  -f|--fps 30             Set Frames Per Second [12-60]"
    echo "  -c|--crf 10             Set Constant Rate Factor [0-51] (Lower is \
                                      better quality)"
    echo "  -q|--qmax 10            Set Maximum Quantization [1-31] (Lower is \
                                      better quality)"
    echo "  -p|--preset ultrafast   Set preset (x264 --help for preset list)"
    echo "  -k|--keep false         Keep original mkv file after conversion \
                                      [true|false]"
    echo "  -o|--output file.webm   Output webm file"
    echo -e "\n"; exit 0
fi

OPTS=`getopt -o m:s:t:o:f:c:p:q:k: --long mode:,space:,title:,output:,fps:,crf:,preset:,qmax:,keep: -- "$@"`
eval set -- "$OPTS"

# If any arguments were given, check what they are and act appropriately.
if [[ $# -ne 0 ]]; then
    while :; do
        case "$1" in
            -m|--mode)   MODE="$2";   shift 2;;
            -s|--space)  MARGIN="$2"; shift 2;;
            -o|--output) OUTPUT="$2"; shift 2;;
            -p|--preset) PRESET="$2"; shift 2;;
            -f|--fps)    FPS="$2";    shift 2;;
            -c|--crf)    CRF="$2";    shift 2;;
            -q|--qmax)   QMAX="$2";   shift 2;;
            -t|--title)  TITLEBAR="$2" shift 2;;
            --)          shift; break;;
            *) echo "Internal error!"; exit 1
        esac
    done
fi

# If an error occurs during recording or converting, then this is set to 1,
# otherwise it is its default value of 0. At the end, if the value is not 0,
# then an error is displayed.
ERROR=0

if [[ "$MODE" = "window" ]]; then
    WINFO=$(xwininfo)

    WINX=$(($(echo $WINFO|grep -Po 'Absolute upper-left X: \K[^ ]*')-$MARGIN))
    WINY=$(($(echo $WINFO|grep -Po 'Absolute upper-left Y: \K[^ ]*')-$MARGIN-$TITLEBAR))
    WINW=$(($(echo $WINFO|grep -Po 'Width: \K[^ ]*')+($MARGIN*2)))
    WINH=$(($(echo $WINFO|grep -Po 'Height: \K[^ ]*')+($MARGIN*2+$TITLEBAR)))

    sleep 2 # Wait before beginning the recording.
    msg "Window is now being recorded."

    # Record Window
    ffmpeg -f x11grab -s "$WINW"x"$WINH" -r $FPS -i $DISPLAY"+$WINX,$WINY" -c:v libx264 -preset $PRESET -crf $CRF "$TMP" || ERROR=1
else
    sleep 2 # Wait before beginning the recording.
    msg "Desktop is now being recorded."

    # Record Fullscreen
    ffmpeg -f x11grab -s $FULLSCREEN -r $FPS -i $DISPLAY -c:v libx264 -preset $PRESET -crf $CRF "$TMP" || ERROR=1
fi

msg "Desktop is no longer being recorded. Video is now being converted to webm."

# Convert Video: mkv -> webm
ffmpeg -i "$TMP" -c:v libvpx -qmin 1 -qmax $QMAX -an -threads 0 -slices 8 -auto-alt-ref 1 -lag-in-frames 16 -pass 1 -f webm /dev/null -y || ERROR=1
ffmpeg -i "$TMP" -c:v libvpx -qmin 1 -qmax $QMAX -an -threads 0 -slices 8 -auto-alt-ref 1 -lag-in-frames 16 -pass 2 "$OUTPUT".webm -y || ERROR=1

# Remove the log file as part of the cleanup.
rm ffmpeg2pass*.log

# Remove the temporary video file location.
rm -f "$TMP"

if [[ "$ERROR" -eq 0 ]]; then
    echo "Recording and Converting has finished and ${OUTPUT}.webm has been"\
         "successfully created."

    msg "Video was successfully converted to webm!"

    exit 0
else
    echo "An unexpected error occurred and recording+converting could not"\
         "complete."

    msg "An unexpected error prevented the video conversion to complete."

    exit 1
fi
