# deskcap.sh

### Usage

Either just:

```
$ /path/to/deskcap.sh
```

or you can use some options found in:

```
$ /path/to/deskcap.sh --help
```

Files are by default saved in `~` under a filename of the current timestamp in
a slightly modified ISO format.
